package valiit.week5;

public class AdditionalExercises1 {
    public static void main(String[] args) {

        System.out.println("Ex1");
        double myProductNetPrice = stripVat(45.69); // Funktsiooni käivitamine
        System.out.printf("Toode letihinnaga %.2f omab netohinda %.2f",
                45.69, myProductNetPrice);

        System.out.println("Ex2");
        System.out.println(isPalindrome("Aias sadas saia"));
        System.out.println(isPalindrome("Lorem ipsum ipsum"));

        System.out.println("Ex3");
        float bmi = calculateBmi(86.9F, 183);
        System.out.printf(
                "Inimene pikkusega %d cm ja kehakaaluga %.2f kg omab kehamassiindeksit %.2f.\n",
                183, 86.9F, bmi);
        System.out.println("Kõnealuse inimese kehatüüp: " + deriveBodyType(bmi));
    }

    // Funktsiooni defineerimine
    private static double stripVat(double initialPrice) {
        double netPrice = initialPrice / 1.2;
        return netPrice;
    }

    private static boolean isPalindrome(String text) {
        if (text == null || text.trim().length() == 0) {
            return false;
        }

        // Sõna tagurpidi keeramine...
        String reversedText = "";

//        // Variant 1
////        for (int i = 0; i < text.length(); i++) {
////            reversedText = text.charAt(i) + reversedText;
////        }

        // Variant 2
        StringBuilder builder = new StringBuilder(text);
        reversedText = builder.reverse().toString();

        boolean isPalindrome = text.equalsIgnoreCase(reversedText);
        return isPalindrome;
    }

    private static float calculateBmi(float weightKg, int heightCm) {
        float heightM = (float) (heightCm / 100.0);
        float bmi = (float)(weightKg / (Math.pow(heightM, 2)));
        return bmi;
    }

    private static String deriveBodyType(float bmi) {
        if (bmi < 16) {
            return "Tervisele ohtlik alakaal";
        } else if (bmi < 19) {
            return "Alakaal";
        } else if (bmi < 25) {
            return "Normaalkaal";
        } else if (bmi < 30) {
            return "Ülekaal";
        } else if (bmi < 35) {
            return "Rasvumine";
        } else if (bmi < 40) {
            return "Tugev rasvumine";
        } else {
            return "Tervisele ohtlik rasvumine";
        }
    }
}

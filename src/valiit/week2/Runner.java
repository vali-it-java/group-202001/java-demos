package valiit.week2;

public class Runner extends Athlete {

    public Runner(String firstName, String lastName, int age, String gender, int height, double weight) {
        super(firstName, lastName, age, gender, height, weight);
    }

    @Override
    public String perform() {
        String x = "Running fast fast fast...";
        System.out.println(x);
        return x;
    }
}

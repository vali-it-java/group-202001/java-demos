package valiit.week2;

public class Human {
    private String name;
    private int age;
    private static int humanCount = 0; // algväärtustamine programmi käivitamisel

    public static int getHumanCount() {
        return humanCount;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private Human(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public static Human makeHuman(String name, int age) {
        Human human = new Human(name, age);
        humanCount++;
        return human;
    }
}

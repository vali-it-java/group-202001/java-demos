package valiit.week2.dogs;

public class LatvianDog extends Dog {
    @Override
    public void bark() {
        System.out.println("vau-vau (Latvian Dog)");
    }
}

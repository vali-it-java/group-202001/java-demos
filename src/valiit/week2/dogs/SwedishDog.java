package valiit.week2.dogs;

public class SwedishDog extends Dog {
    @Override
    public void bark() {
        System.out.println("voff-voff (Swedish Dog)");
    }
}

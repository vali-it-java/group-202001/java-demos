package valiit.week2.dogs;

public class DogManager {
    public static void main(String[] args) {
        Dog dog1 = new LatvianDog();
        Dog dog2 = new SwedishDog();
        Dog dog3 = new JapaneseDog();

        dog1.bark();
        dog2.bark();
        dog3.bark();
    }
}

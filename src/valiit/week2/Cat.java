package valiit.week2;

public class Cat {
    public String name;

    public Cat() {

    }

    public Cat(String name) {
        this.name = name;
    }

    public void makeNoise() {
        System.out.println("Cat with name " + this.name + " makes MJÄÄÄUUU!");
    }
}

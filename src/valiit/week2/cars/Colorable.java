package valiit.week2.cars;

public interface Colorable {
    void setColor(String color);
    String getColor();
}

package valiit.week2.cars;

public interface Movable {
    void move();
    void speedUp(int finalSpeed);
    void slowDown(int finalSpeed);
    String getManufacturer();
    String getModel();
    int getYearOfProduction();
}

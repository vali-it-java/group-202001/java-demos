package valiit.week2.cars;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Movable opel1 = new OpelOmega();
        Movable opel2 = new OpelOmega();
        Movable opel3 = new OpelOmega();
        Colorable colorable1 = new OpelOmega();

        Vehicle ferrari1 = new FerrariF355();
        Movable ferrari2 = new FerrariF355();
        Movable ferrari3 = new FerrariF355();
        Movable ferrari4 = new FerrariF355();

        Vehicle v1 = new FerrariF355();
        v1.setMaxSpeed(320);
        Vehicle v2 = new FerrariF355();
        v2.setMaxSpeed(315);
        Vehicle v3 = new FerrariF355();
        v3.setMaxSpeed(310);
        Vehicle v4 = new OpelOmega();
        v4.setMaxSpeed(160);
        Vehicle v5 = new OpelOmega();
        v5.setMaxSpeed(165);
        Vehicle v6 = new OpelOmega();
        v6.setMaxSpeed(168);
        Vehicle v7 = new OpelOmega();
        v7.setMaxSpeed(210);
        Vehicle v8 = new OpelOmega();
        v8.setMaxSpeed(134);
        Vehicle v9 = new OpelOmega();
        v9.setMaxSpeed(115);
        Vehicle v10 = new OpelOmega();
        v10.setMaxSpeed(229);

        List<Vehicle> vehicles = new ArrayList<>(
                Arrays.asList(v1, v2, v3, v4, v5, v6, v7, v8, v9, v10)
        );
        CarSortingInstruction instruction = new CarSortingInstruction();
        // Variant 1
//        Collections.sort(vehicles, instruction);
        // Variant 2
//        vehicles.sort(instruction);
        // Variant 3
//        vehicles.sort((x1, x2) -> x1.getMaxSpeed() - x2.getMaxSpeed());
//        vehicles.sort(Comparator.comparingInt(Vehicle::getMaxSpeed));

        // "Native" sorting of objcts
        Collections.sort(vehicles);

        System.out.println(vehicles);
    }
}

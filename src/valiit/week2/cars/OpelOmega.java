package valiit.week2.cars;

public class OpelOmega
        implements Movable, Colorable, Vehicle {
    private String color;
    private int maxSpeed;

    @Override
    public void move() {
        System.out.println("Moving slowly...");
    }

    @Override
    public void speedUp(int finalSpeed) {
        System.out.println("Increasing speed slowly... until " + finalSpeed);
    }

    @Override
    public void slowDown(int finalSpeed) {
        System.out.println("slowing down slowly.... until " + finalSpeed);
    }

    @Override
    public String getManufacturer() {
        return "Opel";
    }

    @Override
    public String getModel() {
        return "Omega";
    }

    @Override
    public int getYearOfProduction() {
        return 1986;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public String toString() {
        return "OpelOmega{" +
                "color='" + color + '\'' +
                ", maxSpeed=" + maxSpeed +
                '}';
    }

    @Override
    public int compareTo(Vehicle vehicle) {
        return this.getMaxSpeed() - vehicle.getMaxSpeed();
    }
}

package valiit.week2.cars;

public class FerrariF355 implements Movable, Colorable, Vehicle {
    private String color;
    private int maxSpeed;

    @Override
    public void move() {
        System.out.println("Moving fast...");
    }

    @Override
    public void speedUp(int finalSpeed) {
        System.out.println("Speeding up fast until " + finalSpeed);
    }

    @Override
    public void slowDown(int finalSpeed) {
        System.out.println("Slowing down fast until " + finalSpeed);
    }

    @Override
    public String getManufacturer() {
        return "Ferrari";
    }

    @Override
    public String getModel() {
        return "F355";
    }

    @Override
    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public int getYearOfProduction() {
        return 1994;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "FerrariF355{" +
                "color='" + color + '\'' +
                ", maxSpeed=" + maxSpeed +
                '}';
    }

    @Override
    public int compareTo(Vehicle vehicle) {
        return this.getMaxSpeed() - vehicle.getMaxSpeed();
    }

}

package valiit.week2.cars;

public interface Vehicle extends Comparable<Vehicle> {
    String getManufacturer();
    String getModel();
    void setMaxSpeed(int maxSpeed);
    int getMaxSpeed();
}

package valiit.week2.cars;

import java.util.Comparator;

public class CarSortingInstruction implements Comparator<Vehicle> {

    @Override
    public int compare(Vehicle v1, Vehicle v2) {
        // Selles meetodis küsitakse, kas kaks objekti on juba õiges järjekorras.
        // Kui on õiges järjekorras, siis tagasta number, mis on väiksem, kui 0
        // Kui on vales järjekorras, siis tagasta number, mis on suurem, kui 0
        // Kui objektid on semantiliselt "võrdsed", siis tagasta 0

        // Variant 1
//        if (v1.getMaxSpeed() < v2.getMaxSpeed()) {
//            return -1;
//        } else if (v1.getMaxSpeed() == v2.getMaxSpeed()) {
//            return 0;
//        } else {
//            return 1;
//        }

        // Variant 2
        return v1.getMaxSpeed() - v2.getMaxSpeed();
    }
}

package valiit.week2;

public class CountryInfo {
    public String countryName;
    public String capital;
    public String primeMinister;
    public String[] languages;

    public CountryInfo(String countryName, String capital,
                       String primeMinister, String[] languages) {
        this.countryName = countryName;
        this.capital = capital;
        this.primeMinister = primeMinister;
        this.languages = languages;
    }

    @Override
    public String toString() {
        String info = this.countryName + "\n";
        info = info + this.capital + "\n";
        info = info + this.primeMinister + "\n";
        if (this.languages != null) {
            info = info + "Languages:\n";
            for (String language : this.languages) {
                info = info + "\t" + language + "\n";
            }
        }
        return info;
    }
}

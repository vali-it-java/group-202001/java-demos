package valiit.week2;

public class Day06OOPExercises {
    public static void main(String[] args) {
        // Ülesanne 2, Ülesanne 3
        CountryInfo estonia = new CountryInfo(
                "Estonia", "Tallinn", "Jüri Ratas",
                new String[]{"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"}
        );

        CountryInfo finland = new CountryInfo(
                "Finland", "Helsinki", "Sanna Marin",
                new String[]{"Finnish", "Estonian"}
        );

        CountryInfo sweden = new CountryInfo(
                "Sweden", "Stockholm", "Stefan Löfven",
                new String[]{"Swedish", "Finnish", "Estonian"}
        );

        CountryInfo[] countries = {estonia, finland, sweden};

        for (CountryInfo country : countries) {
            System.out.println(country);
//            System.out.println(country.countryName);
//            System.out.println(country.capital);
//            System.out.println(country.primeMinister);
//            System.out.println("Languages:");
//            for (String language : country.languages) {
//                System.out.println("\t" + language);
//            }
        }
    }
}

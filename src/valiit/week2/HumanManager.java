package valiit.week2;

public class HumanManager {
    public static void main(String[] args) {
        Human human1 = Human.makeHuman("Mari", 56);
        Human human2 = Human.makeHuman("Malle", 31);
        Human human3 = Human.makeHuman("Merje", 24);
        Human human4 = Human.makeHuman("Mihkel", 39);
        System.out.println("Inimesi on " + Human.getHumanCount());
    }
}

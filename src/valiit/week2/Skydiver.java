package valiit.week2;

public class Skydiver extends Athlete {

    public Skydiver(String firstName, String lastName, int age, String gender, int height, double weight) {
        // Baasklassi konstruktori väljakutsumine
        super(firstName, lastName, age, gender, height, weight);
    }

    @Override
    public String perform() {
        String x = "Falling from sky...";
        System.out.println(x);
        return x;
    }

    public void openParachute() {
        System.out.println("Opening...");
    }



}

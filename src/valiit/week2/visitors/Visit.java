package valiit.week2.visitors;

public class Visit {
    private String date;
    private int count;

    public String getDate() {
        return date;
    }

    public int getCount() {
        return count;
    }

    public Visit(String date, int count) {
        this.date = date;
        this.count = count;
    }
}

package valiit.week2.visitors;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
    private static List<Visit> visits = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        List<String> visitStrings =
                Files.readAllLines(Paths.get(args[0]));

        for (int i = 0; i < visitStrings.size(); i++) {
            String[] parts = visitStrings.get(i).split(", ");
            String date = parts[0];
            int count = Integer.parseInt(parts[1]);
            Visit visit = new Visit(date, count);
            visits.add(visit);
        }

        VisitComparingInstruction instruction = new VisitComparingInstruction();
        Collections.sort(visits, instruction);

        System.out.println("Külastused sorteerituna külastajate arvu järgi:");
        for (Visit v : visits) {
            System.out.printf("Päev: %s, külastusi: %d\n", v.getDate(), v.getCount());
        }

        System.out.println("Kõige rohkem oli külastusi:");
        Visit visit = visits.get(visits.size() - 1);
        System.out.printf("Kuupäev: %s; sellel päeval külastas loodusparki %d inimest\n",
                visit.getDate(), visit.getCount());
    }
}

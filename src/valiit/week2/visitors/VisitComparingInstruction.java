package valiit.week2.visitors;

import java.util.Comparator;

public class VisitComparingInstruction implements Comparator<Visit> {
    @Override
    public int compare(Visit v1, Visit v2) {
        return v1.getCount() - v2.getCount();
    }
}

package valiit.week2.bank;

public class Account {
    private String firstName;
    private String lastName;
    private String number;
    private double balance;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getNumber() {
        return number;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Account(String firstName, String lastName, String number, double balance) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.number = number;
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", number='" + number + '\'' +
                ", balance=" + balance +
                '}';
    }
}

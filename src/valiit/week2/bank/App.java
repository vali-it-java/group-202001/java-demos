package valiit.week2.bank;

import java.io.IOException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        try {
            AccountService.loadAccounts(args[0]);
            Scanner scanner = new Scanner(System.in);
            System.out.println("SUPER BANK APP");
            String input;
            do {
                System.out.println("Insert command:");
                input = scanner.nextLine();
                input = input.trim().toLowerCase();
                if (input.startsWith("balance")) {
                    String[] parts = input.split(" ");
                    if (parts.length == 2) { // Otsing konto numbri järgi
                        String accountNumber = parts[1].trim();
                        Account account = AccountService.findAccount(accountNumber);
                        displayAccount(account);
                    } else if (parts.length == 3) { // Otsing nime järgi
                        String firstName = parts[1].trim();
                        String lastName = parts[2].trim();
                        Account account = AccountService.findAccount(firstName, lastName);
                        displayAccount(account);
                    }
                } else if (input.startsWith("transfer")) {
                    String[] parts = input.split(" ");
                    String fromAccount = parts[1].trim();
                    String toAccount = parts[2].trim();
                    double sum = Double.parseDouble(parts[3].trim());
                    TransferResult result = AccountService.transfer(fromAccount, toAccount, sum);
                    if (result.isSuccess()) {
                        System.out.println("Transfer completed!");
                        System.out.println("Sender...");
                        displayAccount(result.getFromAccount());
                        System.out.println("Beneficiary...");
                        displayAccount(result.getToAccount());
                    } else {
                        System.out.println("ERROR!");
                        System.out.println(result.getError());
                    }
                }
            } while (!input.equalsIgnoreCase("exit"));
            System.out.println("Good bye!");
        } catch(BankAccountsFileNotFoundException e) {
            System.out.println("Bank App could not find the accounts file. Please specify the correct file path!");
            System.out.println("----------------");
            System.out.println("DETAILS:");
            System.out.println(e.toString());
            System.out.println("TIME: " + e.getCreationTime());
            System.out.println("----------------");
        }
    }

    private static void displayAccount(Account account) {
        if (account != null) {
            System.out.println("----------------");
            System.out.println("ACCOUNT DETAILS");
            System.out.println("----------------");
            System.out.println("FIRST NAME: " + account.getFirstName());
            System.out.println("LAST NAME: " + account.getLastName());
            System.out.println("NUMBER: " + account.getNumber());
            System.out.println("BALANCE: " + account.getBalance());
            System.out.println("----------------");
        } else {
            System.out.println("Account not found.");
        }
    }
}

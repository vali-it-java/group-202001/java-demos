package valiit.week2.bank;

public class TransferResult {
    private boolean success = false;
    private String error = "";
    private Account fromAccount;
    private Account toAccount;

    public boolean isSuccess() {
        return success;
    }

    public String getError() {
        return error;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public TransferResult(boolean success, String error) {
        this.success = success;
        this.error = error;
    }

    public TransferResult(boolean success, String error, Account fromAccount, Account toAccount) {
        this.success = success;
        this.error = error;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
    }
}

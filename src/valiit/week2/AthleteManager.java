package valiit.week2;

public class AthleteManager {
    public static void main(String[] args) {
        Skydiver skydiver1 = new Skydiver(
                "Teet", "Kask", 45,
                "M", 179, 89
        );
        makeAthletePerformIntensively(skydiver1);

//        Athlete skydiver2 = new Skydiver(
//                "Mari", "Lill", 45,
//                "N", 171, 59
//        );
//        Athlete skydiver3 = new Skydiver(
//                "Neeme", "Murakas", 25,
//                "M", 169, 81
//        );
//        System.out.println(skydiver1);
//        skydiver1.perform();
//        System.out.println(skydiver2);
//        skydiver2.perform();
//        System.out.println(skydiver3);
//        skydiver3.perform();
//
//        Athlete runner1 = new Runner(
//                "Meelis", "Muk", 29, "M", 176, 77);
//        Athlete runner2 = new Runner(
//                "Liina", "Laud", 35, "N", 165, 67);
//        Athlete runner3 = new Runner(
//                "Leeni", "Lukk", 34, "N", 177, 57);
//
//        System.out.println(runner1);
//        runner1.perform();
//        System.out.println(runner2);
//        runner2.perform();
//        System.out.println(runner3);
//        runner3.perform();
    }

    private static void makeAthletePerformIntensively(Athlete a) {
        a.perform();
        a.perform();
        a.perform();
        a.perform();
        a.perform();
    }
}

package valiit.week2;

public class Day06Methods {
    public static void main(String[] args) {
//        double i = 2 * Math.random();
        String myName = "Marek";
        printHello(myName);
        printHello("Rein");
        printHello("Ben");

        String processedText = repeatX("Tere ", 10);
        System.out.println(processedText);

        printNumbersDesc(20);
    }

    // Parameeter - funktsiooni päises defineeritud muutuja
    // Argument - reaalne väärtus, mille annan funktsiooni käivitamisel parameetrile

    public static void printHello(String name) {
        System.out.println("Tere, " + name + "!");
    }

    public static String repeatX(String text, int repeatCount) {
        // "Tere " --> "Tere Tere Tere" (eeldusel, et repeatCount on 3)
        String repeatedText = text.repeat(repeatCount);
        return repeatedText;
    }

    public static String repeatX(String text1, String text2, int repeatCount) {
        return null;
    }

    // Function overloading - ühes klassis on mitu sama nimega, aga erinevate
    //                          parameetritega funktsiooni

    public static void printNumbersDesc(int number) {
        System.out.println(number);
        if (number > 1) {
            printNumbersDesc(--number);
        }
    }

}

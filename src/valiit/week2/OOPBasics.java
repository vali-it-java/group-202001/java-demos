package valiit.week2;

public class OOPBasics {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Miisu");
        //cat1.name = "Miisu";
        Cat cat2 = new Cat();
        cat2.name = "Mihkel";
        Cat cat3 = new Cat();
        cat3.name = "Pätu";
//        cat3 = cat1;

        cat1.makeNoise();
        cat2.makeNoise();
        cat3.makeNoise();
    }
}

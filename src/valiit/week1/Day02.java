package valiit.week1;

public class Day02 {

    public static void main(String[] args) {

        String myText = "See \\ on mingi tekst.\nü\tõüõüõÕÜÜÕÜÕÖÄÖÄž: \"See on otsekõne\"";
        System.out.println(myText);

        String t1 = "tere";
        String t2 = new String("Tere");

        boolean areTextsEqual = t1.equalsIgnoreCase(t2);
        System.out.println(areTextsEqual);

        t2 = t1 + t2;
    }
}

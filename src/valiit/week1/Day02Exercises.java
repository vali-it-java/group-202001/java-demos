package valiit.week1;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Day02Exercises {

    public static void main(String[] args) {

        // Ülesanne 1
        double d = 456.78;
        float f = 456.78F;

        String text = "test";

        int[] number = { 5, 91, 304, 405 };

        double[] decimals = { 56.7, 45.8, 91.2 };

        char a1 = 'a';
        String a2 = "a";

        BigInteger myBigInteger;
        BigDecimal muBigDecimal;

        BigInteger myLargeNumber =
                new BigInteger("7676868683452352345324534534523453245234523452345234523452345");
        BigInteger mySecondNumber =
                new BigInteger("1");
        BigInteger mySum = myLargeNumber.add(mySecondNumber);
        System.out.println(mySum);
    }
}

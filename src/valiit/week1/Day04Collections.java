package valiit.week1;

import java.util.*;

public class Day04Collections {
    public static void main(String[] args) {

        // ---------
        // LIST
        // ---------
//        List myList1 = new ArrayList();
//        myList1.add("abc");
//        myList1.add(11);
//        myList1.add(false);
//
//        String myText = (String) myList1.get(0);
//        int myNumber = (Integer) myList1.get(1);

        List<String> names = new ArrayList<>();
        names.add("Teet");
        names.add("Priit");
        names.add("11");

        String firstName = names.get(0);
        names.remove(1);
        names.set(1, "Axel");
        names.remove("Axel");

        names.add("Pille");
        names.add("Kalle");
        names.add("Kaur");
        names.add("Rene");

        System.out.println(names);

        System.out.println(names.indexOf("Malle"));
        System.out.println(names.indexOf("Kaur"));
        System.out.println(names.contains("Marko"));
        System.out.println(names.contains("Rene"));

        System.out.println(names);
        names.add(2, "Mari");
        System.out.println(names);

//        for(String name : names) {
//            if (name.length() > 4) {
//                System.out.println(name);
//            }
//        }

        names.stream().filter(n -> n.length() > 4).forEach(System.out::println);

        // Halb stiil!
//        List names2 = new ArrayList();
//        names2.add("Pille");
//        names2.add("Kalle");
//        names2.add("Kaur");
//        names2.add("Rene");
//        names2.add(1);
//        names2.stream().filter(n -> ((String)n).length() > 4).forEach(System.out::println);

        // ---------
        // SET
        // ---------
        Set<String> namesSet = new HashSet<>();
        namesSet.add("Kaur");
        namesSet.add("Megan");
        namesSet.add("Toomas");
        namesSet.add("Rainer");
        namesSet.add("Ruudi");
        namesSet.add("Kaur"); // Teist korda sama elemendi lisamine ei õnnestu!

        System.out.println(namesSet);
        for(String name : namesSet) {
            System.out.println("Nimi: " + name);
        }

        // Tahan kindlalt saada kolmandat elementi!
        String[] namesSetArray = namesSet.toArray(String[]::new);
        System.out.println(namesSetArray[2]);

        // ---------
        // MAP
        // ---------
        Map<String, String> estEngDict = new HashMap<>();
        estEngDict.put("auto", "car");
        estEngDict.put("lennuk", "plane");
        estEngDict.put("maja", "house");
        estEngDict.put("isik", "person");
        System.out.println(estEngDict);
        System.out.println(estEngDict.get("maja"));
        System.out.println(estEngDict.size());
        System.out.println(estEngDict.keySet());
        System.out.println(estEngDict.values());
        estEngDict.remove("maja");
        estEngDict.replace("auto", "vehicle");
        System.out.println(estEngDict);
    }
}

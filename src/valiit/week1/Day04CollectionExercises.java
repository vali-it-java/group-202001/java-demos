package valiit.week1;

import java.util.*;

public class Day04CollectionExercises {
    public static void main(String[] args) {
        // Ülesanne 19
//        List<String> cities = new ArrayList<>();
//        cities.add("Baden-Baden");
//        cities.add("Frankfurt");
//        cities.add("Köln");
//        cities.add("Berlin");
//        cities.add("Fürth");
//        List<String> cities =
//                new ArrayList<>(Arrays.asList("Baden-Baden", "Frankfurt", "Köln", "Berlin", "Fürth"));
        List<String> cities =
                Arrays.asList("Baden-Baden", "Frankfurt", "Köln", "Berlin", "Fürth");
        // Esimene linn
        System.out.println(cities.get(0));
        // Kolmas linn
        System.out.println(cities.get(2));
        // Viimane linn
        System.out.println(cities.get(cities.size() - 1));

        // Ülesanne 21
//        Set<String> countries = new TreeSet<>();
//        countries.add("Eritrea");
//        countries.add("Kongo");
//        countries.add("Pakistan");
//        countries.add("Australia");
//        countries.add("Norway");
        Set<String> countries = new TreeSet<>(
                Arrays.asList("Eritrea", "Kongo", "Pakistan", "Australia", "Norway"));

        System.out.println(countries);
//        countries.forEach(c -> System.out.println(c));
//        countries.forEach(System.out::println);
//        for(String country : countries) {
//            System.out.println(country);
//        }
        String[] countriesArr = countries.toArray(String[]::new);
        for (int i = 0; i < countriesArr.length; i++) {
            System.out.println(countriesArr[i]);
        }

        // Ülesanne 22
        Map<String, String[]> countryCities = new HashMap<>();
        countryCities.put("Estonia", new String[]{"Tallinn", "Tartu", "Valga", "Võru"});
        countryCities.put("Sweden", new String[]{"Stockholm", "Uppsala", "Lund", "Köping"});
        countryCities.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"});

        // Variant 1: for
//        String[] countryKeys = countryCities.keySet().toArray(String[]::new);
//        for(int i = 0; i < countryKeys.length; i++) {
//            System.out.println("Country: " + countryKeys[i]);
//            System.out.println("Cities:");
//            String[] currentCountryCities = countryCities.get(countryKeys[i]);
//            for(int j = 0; j < currentCountryCities.length; j++) {
//                System.out.println("\t" + currentCountryCities[j]);
//            }
//        }

        // Variant 2: foreach
        for (String currentCountry : countryCities.keySet()) {
            System.out.println("Country: " + currentCountry);
            System.out.println("Cities:");
            for (String currentCity : countryCities.get(currentCountry)) {
                System.out.println("\t" + currentCity);
            }
        }

        // Ülesanne 20 (LinkedList)
        Queue<String> persons = new LinkedList<>();
        persons.add("Teet");
        persons.add("Mari");
        persons.add("Malle");
        persons.add("Elmar");

//        String name = persons.remove();
//        System.out.println(name);
//        System.out.println(persons.remove()); // Tagastab järjekorras esimese elemendi ja eemaldab selle queue-st.
//        System.out.println(persons.peek()); // Tagastab järjekorras esimese elemendi, ei eemalda midagi.
//        System.out.println(persons.peek());
//        System.out.println(persons.poll());
//        System.out.println(persons.poll()); // Töötab, nagu .peek(), kui elementi pole, tagastab null.
//        System.out.println(persons.poll());

        while(!persons.isEmpty()) {
            System.out.println(persons.remove());
        }
    }
}

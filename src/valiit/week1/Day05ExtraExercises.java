package valiit.week1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day05ExtraExercises {
    public static void main(String[] args) {

        System.out.println("Ülesanne 1");
        System.out.println("Muster 1");

        for(int i = 1; i <= 8; i++) {
            for(int j = 1; j <= 19; j++) {
//                // Variant 1
//                if (i % 2 == 1) {
//                    // Paaritud read
//                    if (j % 2 == 1) {
//                        System.out.print("#");
//                    } else {
//                        System.out.print("+");
//                    }
//                } else {
//                    // Paaris read
//                    if (j % 2 == 0) {
//                        System.out.print("#");
//                    } else {
//                        System.out.print("+");
//                    }
//                }

//                // Variant 2
//                if ((i + j) % 2 == 0) {
//                    System.out.print("#");
//                } else {
//                    System.out.print("+");
//                }

                // Variant 3
                System.out.print((i + j) % 2 == 0 ? "#" : "+");
            }
            System.out.println();
        }

        System.out.println("Muster 2");
        for(int i = 1; i <= 6; i++) {
            for (int j = 1; j <= 6; j++) {
                if (j < i) {
                    System.out.print("-");
                } else if (j == i) {
                    System.out.print("#");
                }
            }
            System.out.println();
        }

        System.out.println("Ülesanne 2");
        String personsStr = "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti; Eesnimi: Mari, Perenimi: Tamm, Vanus: 56, Amet: kosmonaut, Kodakondsus: Soome; Eesnimi: Kalle, Perenimi: Kuul, Vanus: 38, Amet: arhitekt, Kodakondsus: Läti; Eesnimi: James, Perenimi: Cameron, Vanus: 56, Amet: riigiametnik, Kodakondsus: UK; Eesnimi: Donald, Perenimi: Trump, Vanus: 73, Amet: kinnisvaraarendaja, Kodakondsus: USA;";

        System.out.println("Stringide massiiv:");
        personsStr = personsStr.replace(" ", "");
        String[] personsStrArr = personsStr.split(";");
        String[][] persons = new String[personsStrArr.length][];
        for(int i = 0; i < personsStrArr.length; i++) {
            persons[i] = personsStrArr[i].split(",");
            for (int j = 0; j < persons[i].length; j++) {
                persons[i][j] = persons[i][j].split(":")[1];
            }
        }

        System.out.println("List");
        List<String[]> personsList = new ArrayList<>();
        for(String[] person : persons) {
            personsList.add(person);
        }

        System.out.println("Map");
        Map<String, String[]> personsMap = new HashMap<>();
        for(String[] person : persons) {
            personsMap.put(person[0] + " " + person[1], person);
        }

        System.out.println("List of Person objects");
        List<Person> personsList2 = new ArrayList<>();
        for(String[] personArr : persons) {
            personsList2.add(new Person(personArr));
        }
        System.out.println(personsList2);
    }
}

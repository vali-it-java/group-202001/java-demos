package valiit.week1;

public class Person {
    private String firstName;
    private String lastName;
    private int age;
    private String profession;
    private String citizenship;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        // Kontroll: numbreid ei tohi nimes olla
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public Person(String[] personData) {
        this.firstName = personData[0];
        this.lastName = personData[1];
        this.age = Integer.parseInt(personData[2]);
        this.profession = personData[3];
        this.citizenship = personData[4];
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", profession='" + profession + '\'' +
                ", citizenship='" + citizenship + '\'' +
                '}';
    }
}

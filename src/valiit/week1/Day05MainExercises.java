package valiit.week1;

import java.lang.reflect.Array;
import java.util.*;

public class Day05MainExercises {
    public static void main(String[] args) {

        System.out.println("Ülesanne 1");
        for (int i = 1; i <= 6; i++) {
//            for (int j = 1; j <= 6 - i + 1; j++) {
////                System.out.print("#");
////            }
            for (int j = 1; j <= 6; j++) {
                if (j <= 7 - i) {
                    System.out.print("#");
                }
            }
            System.out.println();
        }

        System.out.println("Ülesanne 2");
        // Tekitame read
        for (int i = 1; i <= 6; i++) {
//            // Tekitame veerud
//            for (int j = 1; j <= 6; j++) { // Mitu #-märki tekitame?
//                if (j <= i) {
//                    System.out.print("#");
//                }
//            }
            // Tekitame veerud
            for (int j = 1; j <= i; j++) { // Mitu #-märki tekitame?
                System.out.print("#");
            }
            System.out.println();
        }

        System.out.println("Ülesanne 3");
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 5; j++) {
                if (j <= 5 - i) {
                    System.out.print(" ");
                } else {
                    System.out.print("@");
                }
            }
            System.out.println();
        }

        System.out.println("Ülesanne 4");
        int num1 = 1234567;
        String num1Str = String.valueOf(num1);

        // Variant 1
        char[] num1Chars = num1Str.toCharArray();
        String result = ""; // "7654321"
        for (int i = 0; i < num1Chars.length; i++) {
            result = num1Chars[i] + result;
        }
        int num1Reverse = Integer.parseInt(result); // "7654321" -> 7654321
        System.out.println(num1Reverse);

        // Variant 2
        StringBuilder sb = new StringBuilder(num1Str);
        sb.reverse();
        result = sb.toString();
        num1Reverse = Integer.parseInt(result);
        System.out.println(num1Reverse);

        // Variant 3
        num1Chars = num1Str.toCharArray();
        Stack<String> num1Stack = new Stack<>();
        for (char c : num1Chars) {
            num1Stack.push(String.valueOf(c));
        }
        result = "";
        while (!num1Stack.empty()) {
            result += num1Stack.pop();
        }
        num1Reverse = Integer.parseInt(result);
        System.out.println(num1Reverse);

        System.out.println("Ülesanne 5");

        for (int i = 0; i < args.length; i += 2) {
            String studentName = args[i];
            int studentPoints = Integer.parseInt(args[i + 1]);

            if (studentPoints < 51) {
                System.out.println(studentName + ": FAIL");
            } else if (studentPoints < 61) {
                System.out.printf("%s: PASS - %s, %s\n", studentName, 1, studentPoints);
            } else if (studentPoints < 71) {
                System.out.printf("%s: PASS - %s, %s\n", studentName, 2, studentPoints);
            } else if (studentPoints < 81) {
                System.out.printf("%s: PASS - %s, %s\n", studentName, 3, studentPoints);
            } else if (studentPoints < 91) {
                System.out.printf("%s: PASS - %s, %s\n", studentName, 4, studentPoints);
            } else if (studentPoints < 101) {
                System.out.printf("%s: PASS - %s, %s\n", studentName, 5, studentPoints);
            } else {
                System.out.println("Viga: ebakorrektne punktide arv!");
            }
        }

        System.out.println("Ülesanne 6");
        double[][] triangleSides = {
                {5.6, 2.1},
                {15.65, 34.61},
                {56.6, 2.9},
                {72.54, 98.16},
                {12.54, 42.83}
        };
        for (double[] sides : triangleSides) {
            double a2 = Math.pow(sides[0], 2);
            double b2 = Math.pow(sides[1], 2);
            double c = Math.sqrt(a2 + b2);
            System.out.printf("Kolmnurk külgedega %.2f, %.2f, hüpotenuus: %.2f\n",
                    sides[0], sides[1], c);
        }

        System.out.println("Ülesanne 7");
        String[][] countries = {
                {"Estonia", "Tallinn", "Jüri Ratas"},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
                {"Lithuania", "Vilnius", "Saulius Skvernelis"},
                {"Finland", "Helsinki", "Sanna Marin"},
                {"Sweden", "Stockholm", "Stefan Löfven"},
                {"Norway", "Oslo", "Erna Solberg"},
                {"Denmark", "Copenhagen", "Mette Frederiksen"},
                {"Russia", "Moscow", "Dmitry Medvedev"},
                {"Germany", "Berlin", "Angela Merkel"},
                {"France", "Paris", "Édouard Philippe"}
        };

        for (String[] country : countries) {
            System.out.println(country[2]);
        }

        System.out.println("For...");
        for (int i = 0; i < countries.length; i++) {
            System.out.printf("Country: %s, Capital: %s, Prime minister: %s\n",
                    countries[i][0], countries[i][1], countries[i][2]);
        }

        System.out.println("Foreach...");
        for (String[] country : countries) {
            System.out.printf("Country: %s, Capital: %s, Prime minister: %s\n",
                    country[0], country[1], country[2]);
        }

        // Ülesanne 8
        String[][][] countries2 = {
                {
                        {"Estonia"}, {"Tallinn"}, {"Jüri Ratas"},
                        {"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"},
                },
                {
                        {"Latvia"}, {"Riga"}, {"Arturs Krišjānis Kariņš"},
                        {"Estonian", "Russian", "Latvian", "Belarusian"}
                },
                {
                        {"Lithuania"}, {"Vilnius"}, {"Saulius Skvernelis"},
                        {"Lithuanian", "Russian", "Belarusian", "Finnish"}
                },
                {
                        {"Finland"}, {"Helsinki"}, {"Sanna Marin"},
                        {"Finnisn", "Estonian"}
                },
                {
                        {"Sweden"}, {"Stockholm"}, {"Stefan Löfven"},
                        {"Swedish"}
                },
                {
                        {"Norway"}, {"Oslo"}, {"Erna Solberg"},
                        {"Norwegian", "English"}
                },
                {
                        {"Denmark"}, {"Copenhagen"}, {"Mette Frederiksen"},
                        {"Danish", "Swedish", "Sami"}
                },
                {
                        {"Russia"}, {"Moscow"}, {"Dmitry Medvedev"},
                        {"English", "Russian", "Finnish"}
                },
                {
                        {"Germany"}, {"Berlin"}, {"Angela Merkel"},
                        {"German", "French"}
                },
                {
                        {"France"}, {"Paris"}, {"Édouard Philippe"},
                        {}
                }
        };

        for (String[][] countryInfo : countries2) {
            System.out.println(countryInfo[0][0]);
            String[] currentCountryLanguages = countryInfo[3];
            for (String language : currentCountryLanguages) {
                System.out.println("\t" + language);
            }
        }

        // Alljärgnevalt kaks halba näidet, kuidas mitte programmeerida...

        System.out.println("Näide 2");
        Object[][] countries3 = {
                new Object[]{"Estonia", "Tallinn", "Jüri Ratas", new String[]{"Estonian", "Russian"}},
                new Object[]{"Finland", "Helsinki", "Sanna Marin", new String[]{"Finnish", "Swedish"}}
        };

        for (Object[] countryInfo : countries3) {
            System.out.println(countryInfo[0]);
            for (String x : (String[]) countryInfo[3]) {
                System.out.println("\t" + x);
            }
        }

        System.out.println("Näide 3");
        Object countries4 = new Object[]{
                new Object[]{"Estonia", "Tallinn", "Jüri Ratas", new String[]{"Estonian", "Russian"}},
                new Object[]{"Finland", "Helsinki", "Sanna Marin", new String[]{"Finnish", "Swedish"}}
        };

        for (Object countryInfo : (Object[]) countries4) {
            System.out.println(((Object[]) countryInfo)[0]);
            for (String x : (String[]) (((Object[]) countryInfo)[3])) {
                System.out.println("\t" + x);
            }
        }

        System.out.println("Ülesanne 9");

        System.out.println("Variant 1");
        List<List<List<String>>> countries5 =
                Arrays.asList(
                        Arrays.asList(
                                Arrays.asList("Estonia"),
                                Arrays.asList("Tallinn"),
                                Arrays.asList("Jüri Ratas"),
                                Arrays.asList("Estonian", "Russian", "Finnish")
                        ),
                        Arrays.asList(
                                Arrays.asList("Finland"),
                                Arrays.asList("Helsinki"),
                                Arrays.asList("Sanna Marin"),
                                Arrays.asList("Finnish", "Swedish", "Sami")
                        ),
                        Arrays.asList(
                                Arrays.asList("Sweden"),
                                Arrays.asList("Stockholm"),
                                Arrays.asList("Stefan Löfven"),
                                Arrays.asList("Swedish", "English")
                        )
                );

        for (List<List<String>> info : countries5) {
            System.out.println(info.get(0).get(0));
            List<String> currentCountryLanguages = info.get(3);
            for (String language : currentCountryLanguages) {
                System.out.println("\t" + language);
            }
        }

        System.out.println("Variant 2");
        List<List<String[]>> countries6 =
                Arrays.asList(
                        Arrays.asList(
                                new String[]{"Estonia"},
                                new String[]{"Tallinn"},
                                new String[]{"Jüri Ratas"},
                                new String[]{"Estonian", "Russian", "Finnish"}
                        ),
                        Arrays.asList(
                                new String[]{"Finland"},
                                new String[]{"Helsinki"},
                                new String[]{"Sanna Marin"},
                                new String[]{"Finnish", "Swedish", "Sami"}
                        ),
                        Arrays.asList(
                                new String[]{"Sweden"},
                                new String[]{"Stockholm"},
                                new String[]{"Stefan Löfven"},
                                new String[]{"Swedish", "English"}
                        )
                );

        for (List<String[]> info : countries6) {
            System.out.println(info.get(0)[0]);
            String[] currentCountryLanguages = info.get(3);
            for (String language : currentCountryLanguages) {
                System.out.println("\t" + language);
            }
        }

        System.out.println("Ülesanne 10");
        Map<String, String[][]> countries7 = new HashMap<>();
        countries7.put(
                "Estonia",
                new String[][]{
                        {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian", "Finnish", "Latvian"}
                }
        );
        countries7.put(
                "Latvia",
                new String[][]{
                        {"Riga"}, {"Arturs Krišjānis Kariņš"}, {"Latvian", "Russian", "Estonian"}
                }
        );
        countries7.put(
                "Russia",
                new String[][]{
                        {"Moscow"}, {"Dmitry Medvedev"}, {"Russian", "English"}
                }
        );

        for (String countryName : countries7.keySet()) {
            System.out.println(countryName);
            String[] languages = countries7.get(countryName)[2];
            for (String lang : languages) {
                System.out.println("\t" + lang);
            }
        }

        System.out.println("Ülesanne 11");
        Queue<String[][]> countries8 = new LinkedList<>();
        countries8.add(
                new String[][]{
                        {"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian", "Finnish", "Latvian"}
                }
        );
        countries8.add(
                new String[][]{
                        {"Latvia"}, {"Riga"}, {"Arturs Krišjānis Kariņš"}, {"Latvian", "Russian", "Estonian"}
                }
        );
        countries8.add(
                new String[][]{
                        {"Russia"}, {"Moscow"}, {"Dmitry Medvedev"}, {"Russian", "English"}
                }
        );

        while (!countries8.isEmpty()) {
            String[][] country = countries8.remove();
            System.out.println(country[0][0]);
            String[] languages = country[3];
            for (int i = 0; i < languages.length; i++) {
                System.out.println("\t" + languages[i]);
            }
        }
    }
}

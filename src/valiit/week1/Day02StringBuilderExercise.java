package valiit.week1;

public class Day02StringBuilderExercise {
    public static void main(String[] args) {
        String president1 = "Konstantin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersi Kaljulaid";

        StringBuilder sb = new StringBuilder();
        sb.append(president1);
        sb.append(", ");
        sb.append(president2);
        sb.append(", ");
        sb.append(president3);
        sb.append(", ");
        sb.append(president4);
        sb.append(" ja ");
        sb.append(president5);
        sb.append(" on Eesti presidendid");
        System.out.println(sb.toString());
    }
}

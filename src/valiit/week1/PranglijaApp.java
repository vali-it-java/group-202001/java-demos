package valiit.week1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PranglijaApp {
    private static final int GUESS_COUNT = 3;
    private static List<Result> log = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        String status = "1";
        do {
            System.out.println("Arvutimäng 'Pranglija'");
            System.out.println("Sisesta oma nimi:");
            String playerName = scanner.nextLine();
            System.out.println("Sisesta miinimumnumber:");
            int rangeMin = getNumberFromConsole(0, Integer.MAX_VALUE);
            System.out.println("Sisesta maksimumnumber:");
            int rangeMax = getNumberFromConsole(rangeMin + 1, Integer.MAX_VALUE);
            int rangeDiff = rangeMax + 1 - rangeMin;

            long start = System.currentTimeMillis();
            for (int i = 1; i <= GUESS_COUNT; i++) {
                int num1 = (int) (Math.random() * rangeDiff) + rangeMin;
                int num2 = (int) (Math.random() * rangeDiff) + rangeMin;
                System.out.printf("%d + %d = ", num1, num2);
                int guessedSum = getNumberFromConsole(Integer.MIN_VALUE, Integer.MAX_VALUE);
                if (num1 + num2 != guessedSum) {
                    System.out.println("Mäng läbi, sa kaotasid!");
                    break;
                } else if (i == GUESS_COUNT) {
                    registerResult(playerName, rangeMin, rangeMax, start);
                }
            }

            System.out.println("Mida soovid teha? [0 - välju mängust, 1 - uus mäng]");
            status = scanner.nextLine();
        } while (status.equals("1"));
        System.out.println(log);
        System.out.println("Head aega!");
    }

    private static void registerResult(String playerName, int rangeMin, int rangeMax, long start) {
        long end = System.currentTimeMillis();
        double diff = (end - start) / 1000.0;
        diff = Math.round(diff * 10) / 10.0;
        System.out.println("Tubli! Arvasid " + GUESS_COUNT + " korda õigesti!");
        System.out.printf("Sul kulus selleks %s sekundit.\n", diff);

        Result result = new Result();
        result.name = playerName;
        result.range = rangeMin + " - " + rangeMax;
        result.duration = diff;
        log.add(result);
    }

    public static class Result {
        public String name;
        public String range;
        public double duration;

        @Override
        public String toString() {
            return "Result{" +
                    "name='" + name + '\'' +
                    ", range='" + range + '\'' +
                    ", duration=" + duration +
                    '}';
        }
    }

    private static int getNumberFromConsole(int lowerBound, int upperBound) {
        int i = -1;
        while(true) {
            try {
                i = Integer.parseInt(scanner.nextLine());
                if (i >= lowerBound && i <= upperBound) {
                    return i;
                }
            } catch (NumberFormatException e) {
                System.out.println("Viga: sisend ei ole number!");
            } finally {
                System.out.printf("Sisesta positiivne number vahemikus: %d - %d\n",
                        lowerBound, upperBound);
            }
        }
    }
}

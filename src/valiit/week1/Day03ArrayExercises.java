package valiit.week1;

public class Day03ArrayExercises {
    public static void main(String[] args) {
        // Ülesanne 7
        int[] numbers;
        numbers = new int[5];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = 3;
        numbers[3] = 4;
        numbers[4] = 5;
        System.out.println(numbers[0]);
        System.out.println(numbers[2]);
        System.out.println(numbers[numbers.length - 1]);

        // Ülesanne 8
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Pariis"};

        // Ülesanne 9
        int[][] numbers2 = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9, 0}
        };
        System.out.println(numbers2[1][2]);

        // Ülesanne 10

        // Inline
        String[][] countryCities = {
                {"Tallinn", "Tartu", "Valga", "Võru"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko", "Jämsä"},
        };

        // Element-haaval
        String[][] countryCities2 = new String[3][4];
        countryCities2[0][0] = "Tallinn";
        countryCities2[0][1] = "Tartu";
        countryCities2[0][2] = "Valga";
        countryCities2[0][3] = "Võru";

        countryCities2[1][0] = "Stockholm";
        countryCities2[1][1] = "Uppsala";
        countryCities2[1][2] = "Lund";
        countryCities2[1][3] = "Köping";

        countryCities2[2] = new String[] { "Helsinki", "Espoo", "Hanko", "Jämsä" };
//        countryCities2[2][0] = "Helsinki";
//        countryCities2[2][1] = "Espoo";
//        countryCities2[2][2] = "Hanko";
//        countryCities2[2][3] = "Jämsä";

        System.out.println(countryCities2[1][2]);
        System.out.println(countryCities2[2][0]);
    }
}

package valiit.week1;

public class Day03If {
    public static void main(String[] args) {

        String textToPrint = "";
        switch(args[0]) {
            case "RED":
                textToPrint = "Driver has to stop car and wait for green light.";
                break;
            case "YELLOW":
                textToPrint = "Driver has to be ready to stop the car or to start driving.";
                break;
            case "GREEN":
                textToPrint = "Driver can drive a car.";
                break;
            default:
                textToPrint = "Unknown color, traffic lights is broken!";
        }

        System.out.println(textToPrint);
    }
}

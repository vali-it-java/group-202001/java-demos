package valiit.week1;

public class Day04Loops {
    public static void main(String[] args) {

//        for (int i = 0; i < 100; i++) {
//            System.out.println("Juhtmuutuja väärtus: " + i);
//        }

//        int i = 0;
//        while(i < 100) {
//            System.out.println("Juhtmuutuja väärtus: " + i);
//            i++;
//        }

//        int i = 0;
//        do {
//            System.out.println("Juhtmuutuja väärtus: " + i);
//            i++;
//        } while(i < 100);

//        String[] names = {"Mari", "Malle", "Kati", "Kalle"};
//        for(String name : names) {
//            System.out.println(name);
//        }

        String[] names = {"Mari", "Malle", "Kati", "Kalle"};
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }

        String[][] countries = {
                {"Tallinn", "Tartu", "Valga", "Võru"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko", "Jämsä"},
        };
//        for(String[] cities : countries) {
//            for(String city : cities) {
//                System.out.println(city);
//            }
//        }

        for (int i = 0; i < countries.length; i++) {
            for (int j = 0; j < countries[i].length; j++) {
                System.out.println(countries[i][j]);
            }
        }
    }
}

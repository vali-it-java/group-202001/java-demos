package valiit.week1;

import java.util.*;

public class CollectionToArraySample {
    public static void main(String[] args) {
        Set<String> countries = new TreeSet<>(
                Arrays.asList("Eritrea", "Kongo", "Pakistan", "Australia", "Norway"));

        String[] countriesArray = countries.toArray(String[]::new);
//        Object[] countriesArray = countries.toArray();
//        String country1 = (String)countriesArray[0];

//        String[] countriesArray = new String[countries.size()];
////        int i = 0;
////        for(String country : countries) {
////            countriesArray[i] = country;
////            i++;
////        }


        Iterator i = countries.iterator();
        while(i.hasNext()) {
            System.out.println(i.next());
        }

    }
}

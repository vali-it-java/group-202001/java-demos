package valiit.week1;

public class Day02StringProcessingExercises {
    public static void main(String[] args) {

        // Strings

        // Ülesanne 1
        String helloWorldText1 = "Hello, World!";
        System.out.println(helloWorldText1);

        String helloWorld2 = "Hello \"World\"";
        System.out.println(helloWorld2);

        System.out.println("Steven Hawking once said: \"Life would be tragic if it weren\'t funny\".");

        String complexText = "Kui liita kokku sõned \"See on teksti esimene pool \"" +
                " ning \"See on teksi teine pool\", siis tulemuseks saame " +
                "\"See on teksti esimene pool See on teksti teine pool\"";
        System.out.println(complexText);

        System.out.println("Elu on ilus");
        System.out.println("Elu on \'ilus\'");
        System.out.println("Elu on \"ilus\"");
        System.out.println("Kõige enam tekitab segadust \"-märgi kasutamine sõne sees.");
        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"");
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis.");

        // Ülesanne 2
        String tallinnPopulation = "450 000";
        System.out.println("Tallinnas elab " + tallinnPopulation + " inimest");

        int populationOfTallinn = 450_000;
        String tallinnPopulationText = "Tallinnas elab " + populationOfTallinn + " inimest";
        System.out.println(tallinnPopulationText);

        String tallinnPopText = String.format("Tallinnas elab %,d inimest", populationOfTallinn);
        System.out.println(tallinnPopText);

        String piValueText = String.format("PI väärtuse arvuline suurus on %.3f", Math.PI);
        System.out.println(piValueText);

        // Ülesanne 3
        String bookTitle = "Rehepapp";
        System.out.printf("Raamat \"%s\" autor on Andrus Kivirähk.\n", bookTitle);

        // Ülesanne 4

        String planet1 = "Merkuur";
        String planet2 = "Veenus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uraan";
        String planet8 = "Neptuun";
        int planetCount = 8;

        String planetText = planet1 + ", " + planet2 + ", " + planet3 +
                ", " + planet4 + ", " + planet5 + ", " + planet6 + ", " + planet7 +
                " ja " + planet8 + " on Päikesesüsteemi " + planetCount + " planeeti.";
        System.out.println(planetText);

        String planetText2 =
                String.format(
                        "%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %d planeeti.",
                        planet1, planet2, planet3, planet4, planet5, planet6, planet7,
                        planet8, planetCount
                );
        System.out.println(planetText2);

        String[] planets = {
                "Merkuur", "Veenus", "Maa", "Marss",
                "Jupiter", "Saturn", "Uraan", "Neptuun", "8"
        };

        String planetText3 =
                String.format(
                        "%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %s planeeti.",
                        planets
                );
        System.out.println(planetText3);
    }
}

package valiit.week6;

import java.util.regex.Pattern;

public class Exercises {
    public static void main(String[] args) {

        System.out.println("Ex 11");
        String text = "Lorem   ipsum ipsum \tdolore est.";
        System.out.println("Initial text: " + text);
        System.out.println("Resulting text:");
        System.out.println(convertSpaces(text));

        System.out.println("Ex 12");
        String initialText = "Test";
        String resultingText = cloneCharsInWord(initialText, 3);
        System.out.println("Result: " + resultingText);

        System.out.println("Ex 13");
        initialText = "Auto sõidab maanteel, vasti jõuab kohale!:;,.\"\'";
        System.out.println("Initial text: " + initialText);
        String censoredText = censorSentence(initialText);
        System.out.println("Censored text: " + censoredText);
    }

    private static String convertSpaces(String text) {
        return text.replaceAll("\\s\\s*", "\n");
    }

    private static String cloneCharsInWord(String text, int repeatCount) {
        String resultText = "";

        for (int i = 0; i < text.length(); i++) {
//            String charStr = String.valueOf(text.charAt(i));
            //resultText = resultText + charStr.repeat(repeatCount);
//            for (int j = 0; j < repeatCount; j++) {
//                resultText = resultText + charStr;
//            }
            resultText = resultText + repeatChars(text.charAt(i), repeatCount);
        }

        return resultText;
    }

    private static String repeatChars(char oneChar, int repeatCount) {
        String result = "";

        String charStr = String.valueOf(oneChar);
        for (int j = 0; j < repeatCount; j++) {
            result = result + charStr;
        }

        return result;
    }

    private static String censorSentence(String text) {
        String result = "";

        for (int k = 0; k < text.length(); k++) {
            String charStr = String.valueOf(text.charAt(k));
//          Variant1: Tähed, mida tahaksime tsenseerida: A-Za-zÕÜÖÄõüöä
//            if(Pattern.matches("[a-zA-ZÕÜÖÄõüöä]", charStr)){
//                result = result + "#";
//            } else {
//                result = result + charStr;
//            }

//          Variant2: Märgid, mida ei tahaks tsenseerida: ,.!?"':;
            if(!Pattern.matches("[\\!,\\s\\:\\;\\.\"\']", charStr)){
                    result = result + "#";
                } else {
                    result = result + charStr;
            }
        }

        return result;
    }
}

package valiit.week6;

public class ConvertingApp {
    public static void main(String[] args) {

        UsdEurCurrencyConverter usdEurConverter = new UsdEurCurrencyConverter();
        double sumInEur = 1291.43;
        System.out.println("Summa eurodes: " + sumInEur);
        double sumInUsd = usdEurConverter.fromEuros(sumInEur);
        System.out.println("Summa USA dollarites: " + sumInUsd);
        double sumInEur2 = usdEurConverter.toEuros(sumInUsd);
        System.out.println("Summa eurodes (uuesti): " + sumInEur2);
        System.out.println("Kas summad eurodes on sarnased? " + (sumInEur == sumInEur2));

        System.out.println("----------");

        GbpEurCurrencyConverter gbpEurConverter = new GbpEurCurrencyConverter();
        double sumInEur3 = 999.67;
        System.out.println("Summa eurodes: " + sumInEur3);
        double sumInGbp = gbpEurConverter.fromEuros(sumInEur3);
        System.out.println("Summa Briti naeltes: " + sumInGbp);
        double sumInEur4 = gbpEurConverter.toEuros(sumInGbp);
        System.out.println("Summa eurodes (uuesti): " + sumInEur4);
        System.out.println("Kas summad eurodes on sarnased? " + (sumInEur4 == sumInEur3));
    }

    public static abstract class AbstractCurrencyConverter {

        public abstract double getExchangeRate();

        public double fromEuros(double sumInEur) {
            return this.round(sumInEur * getExchangeRate());
        }

        public double toEuros(double sumInForeignCurrency) {
            return this.round(sumInForeignCurrency / getExchangeRate());
        }

        protected double round(double sum) {
            double roundedSum = Math.round(sum * 100) / 100.0;
            return roundedSum;
        }
    }

    public static class UsdEurCurrencyConverter extends AbstractCurrencyConverter {

        @Override
        public double getExchangeRate() {
            return 1.0901;
        }
    }

    public static class GbpEurCurrencyConverter extends AbstractCurrencyConverter {

        @Override
        public double getExchangeRate() {
            return 0.8433;
        }
    }
}

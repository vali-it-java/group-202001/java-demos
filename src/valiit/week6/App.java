package valiit.week6;

public class App {

    public static void main(String[] args) {

        Period p = getHumanReadiblePeriod(1000_000_000);
        System.out.println(
                "Miljard sekundit on " + p
        );
    }

    private static Period getHumanReadiblePeriod(int periodInSeconds) {
        int years = periodInSeconds / (60 * 60 * 24 * 360);
        int remainingSeconds = periodInSeconds % (60 * 60 * 24 * 360);
        int months = remainingSeconds / (60 * 60 * 24 * 30);
        remainingSeconds = remainingSeconds % (60 * 60 * 24 * 30);
        int days = remainingSeconds / (60 * 60 * 24);
        remainingSeconds = remainingSeconds % (60 * 60 * 24);
        int hours = remainingSeconds / (60 * 60);
        remainingSeconds = remainingSeconds % (60 * 60);
        int minutes = remainingSeconds / 60;
        int seconds = remainingSeconds % 60;
        Period calculatedPeriod = new Period(years, months, days, hours, minutes, seconds);
        return calculatedPeriod;
    }

    public static class Period {
        private int years;
        private int months;
        private int days;
        private int hours;
        private int minutes;
        private int seconds;

        public Period(int years, int months, int days, int hours, int minutes, int seconds) {
            this.years = years;
            this.months = months;
            this.days = days;
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
        }

        @Override
        public String toString() {
            return "Period{" +
                    "years=" + years +
                    ", months=" + months +
                    ", days=" + days +
                    ", hours=" + hours +
                    ", minutes=" + minutes +
                    ", seconds=" + seconds +
                    '}';
        }
    }
}

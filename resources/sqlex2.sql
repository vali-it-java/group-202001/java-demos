
USE countries;

-- REPUBLIC
-- KINGDOM
-- FEDERATION

INSERT INTO country_type (`name`) VALUES ('REPUBLIC');
INSERT INTO country_type (`name`) VALUES ('KINGDOM');
INSERT INTO country_type (`name`) VALUES ('FEDERATION');

-- Eesti
INSERT INTO country (`name`, `population`, `head_of_state`, `gdp_per_capita`, `country_type_id`)
VALUES ('Eesti Vabariik', 1300000, 'Kersti Kaljulaid', 37605, 1);

-- UK
INSERT INTO country (`name`, `population`, `head_of_state`, `gdp_per_capita`, `country_type_id`)
VALUES ('Ühendkuningriik', 65000000, 'Elisabeth II', 65000, 3);

-- Venemaa
INSERT INTO country (`name`, `population`, `head_of_state`, `gdp_per_capita`, `country_type_id`)
VALUES ('Vene Föderatsioon', 145000000, 'Vladimir Putin', 30000, 4);

-- Saksamaa
INSERT INTO country (`name`, `population`, `head_of_state`, `gdp_per_capita`, `country_type_id`)
VALUES ('Saksamaa', 85000000, 'Angela Merkel', 60000, 4);

-- Norra
INSERT INTO country (`name`, `population`, `head_of_state`, `gdp_per_capita`, `country_type_id`)
VALUES ('Norra Kuningriik', 7000000, 'Harald V', 80000, 3);

SELECT * FROM country_type;
SELECT * FROM country;

SELECT * FROM country WHERE NAME = 'Eesti Vabariik';

UPDATE country SET `name` = 'Eesti Kuningriik', head_of_state = 'Tanel Padar'
WHERE `name` = 'Eesti Vabariik';

UPDATE country SET country_type_id = 3 WHERE `name` = 'Eesti Kuningriik';

DELETE FROM country WHERE `name` LIKE 'Eesti%';



-- ex 1
SELECT COUNT(*) AS emp_count FROM employees;

-- ex 2
SELECT COUNT(*) AS sales_rep_count
FROM employees WHERE jobTitle = 'Sales Rep';

-- ex 3
SELECT * FROM employees WHERE jobTitle = 'President';
SELECT * FROM employees WHERE reportsTo IS NULL;

-- ex 4
-- Option 1: sub-query
SELECT firstName, lastName
FROM employees WHERE officeCode IN (
	SELECT o.officeCode
	FROM offices o
	WHERE o.country = 'Japan'
);

-- Option 2: inner-join
SELECT e.firstName, e.lastName
FROM employees e
INNER JOIN offices o ON o.officeCode = e.officeCode
WHERE o.country = 'Japan';

-- ex 5
SELECT e.firstName, o.phone
FROM employees e
INNER JOIN offices o ON o.officeCode = e.officeCode;

-- ex6
-- Option 1: sub-query
SELECT email FROM employees WHERE officeCode IN (
	SELECT o.officeCode FROM offices o WHERE o.country = 'UK'
);

-- Option 2: inner join
SELECT e.email FROM employees e
INNER JOIN offices o ON e.officeCode = o.officeCode
WHERE o.country = 'UK';

-- ex 7
SELECT
	c.customerName AS customer_name,
	CONCAT(e.firstName, ' ', e.lastName) AS sales_rep
FROM customers c
INNER JOIN employees e ON e.employeeNumber = c.salesRepEmployeeNumber;

-- ex 8
SELECT COUNT(1) FROM customers WHERE country = 'USA';

-- ex 9
SELECT COUNT(*) FROM products;

-- ex 10
SELECT productName FROM products WHERE productLine = 'Planes';

-- ex 11
SELECT productName, quantityInStock FROM products WHERE quantityInStock > 9300;

-- ex 12
-- DESC - descending order (kahanevalt)
-- ASC - ascending order (kasvavalt)
SELECT orderNumber, orderDate FROM orders ORDER BY orderDate DESC;

-- ex 13
-- limit
SELECT productName, buyPrice, productLine
FROM products
ORDER BY buyPrice DESC
LIMIT 1;

-- sub-query
SELECT productName, buyPrice, productLine
FROM products
WHERE buyPrice IN (SELECT MAX(buyPrice) FROM products);

-- ex 14
-- Option 1: between keyword
SELECT orderNumber, orderDate
FROM orders
WHERE orderDate BETWEEN '2004-08-01' AND '2004-09-25';

-- Option 2: boolean expressions
SELECT orderNumber, orderDate
FROM orders
WHERE orderDate >= '2004-08-01' AND orderDate <= '2004-09-25';

-- ex 15
SELECT orderNumber, orderDate
FROM orders
WHERE customerNumber IN (
	SELECT customerNumber FROM customers
	WHERE customerName LIKE 'Suominen%'
)
ORDER BY orderDate ASC;

-- ex 16
SELECT
	orderNumber,
	orderDate,
	(
		SELECT
			SUM(orderDetails.quantityOrdered * orderDetails.priceEach)
		FROM
			orderDetails
		WHERE
			orderDetails.orderNumber = orders.orderNumber
	) orderTotalPrice
FROM orders
WHERE customerNumber IN (
	SELECT customerNumber FROM customers
	WHERE customerName LIKE 'Suominen%'
)
ORDER BY orderDate ASC;

-- ex 17
SELECT
	orderNumber,
	(
		SELECT
			SUM(orderDetails.quantityOrdered * orderDetails.priceEach)
		FROM
			orderDetails
		WHERE
			orderDetails.orderNumber = orders.orderNumber
	) orderTotalPrice
FROM orders
ORDER BY orderTotalPrice DESC
LIMIT 1;



